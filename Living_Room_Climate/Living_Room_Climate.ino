#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoOTA.h>

#define mqtt_server "192.168.0.58"
#define mqtt_user "homeassistant"
#define mqtt_password "2032"
#define mqtt_client "livingroom_temphumidity"

#define humidity_topic "sensor/livingroom/humidity"
#define temperature_topic "sensor/livingroom/temperature"

#define DHTTYPE DHT11
#define DHTPIN 5

WiFiClient espClient;
PubSubClient client(espClient);
DHT dht(DHTPIN, DHTTYPE, 11);

void setup() {
  Serial.begin(115200);
  dht.begin();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  setup_OTA();
}

void setup_OTA() {
  ArduinoOTA.setHostname(mqtt_client);
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
    
  ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
  });
  
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
    
  ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(mqtt_client);
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(mqtt_client, mqtt_user, mqtt_password)) {
      Serial.println("connected");
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void publish_message(String topic, String value) {
  Serial.print("Publishing message with value: ");
  Serial.print(value.c_str());
  Serial.print(" to topic: ");
  Serial.println(topic);
  client.publish(topic.c_str(), value.c_str(), true);
}

bool checkBound(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

long lastMsg = 0;
float temp = 0.0;
float hum = 0.0;
float diff = 1.0;
void readTempHumidity() {
  long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;

    float newTemp = dht.readTemperature();
    float newHum = dht.readHumidity();

    if (checkBound(newTemp, temp, diff)) {
      temp = newTemp;
      Serial.print("New temperature:");
      Serial.println(String(temp).c_str());
      int publishTemp = temp;
      publish_message(temperature_topic, String(publishTemp));
    } else {
      Serial.println("Room Temperature unchanged");
    }

    if (checkBound(newHum, hum, diff)) {
      hum = newHum;
      Serial.print("New humidity:");
      Serial.println(String(hum).c_str());
      client.publish(humidity_topic, String(hum).c_str(), true);
      int publishHum = hum;
      publish_message(humidity_topic, String(publishHum));
    } else {
      Serial.println("Room Humidity unchanged");      
    }
  }
}

void loop() {
  ArduinoOTA.handle(); 
  if (!client.connected()) {
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();

  readTempHumidity();  
}
